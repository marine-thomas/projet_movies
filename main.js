const movieList = [
    {
      title: "Avatar",
      releaseYear: 2009,
      duration: 162,
      director: "James Cameron",
      actors: ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
      description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
      poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
      rating: 7.9,
    },
    {
      title: "300",
      releaseYear: 2006,
      duration: 117,
      director: "Zack Snyder",
      actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
      description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
      poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
      rating: 7.7,
    },
    {
      title: "The Avengers",
      releaseYear: 2012,
      duration: 143,
      director: "Joss Whedon",
      actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
      description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
      poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
      rating: 8.1,
    },
];

/*
    Récupérer les informations des films
*/

//Créer dans le DOM une liste pour les films
const list = document.getElementById('movie_list');
const createList = document.createElement('ul');
createList.classList.add('d-flex','flex-row', 'flex-wrap'); //Ajouter une class à la liste
list.appendChild(createList); // Ajouter dans la section qui a pour id movie_list le li

// Créer une fonction qui appelle tous les éléments d'un film
function movieShow(movieToShow) {
    const createListItem = document.createElement('li'); //Créer dans le DOM un élément li
    createListItem.classList.add('d-flex', 'flex-column', 'flex-wrap', 'card', 'listMovie'); // Ajouter à mes li les classes suivantes
    createList.append(createListItem); //Ajouter à la liste ce li
    
    
    const imageMovie = document.createElement('img'); //Créer dans le Dom une balise image
    imageMovie.classList.add('movieImage'); //On rajoute une class à l'image
    const falseImage = 'https://placehold.co/410x440/000000/FFFFFF/png?text=Pensez+à+rajouter+une+image';
    if(movieToShow.poster == '' || movieToShow.poster == undefined){
        imageMovie.src = falseImage;
    } else {
        imageMovie.src = movieToShow.poster; //L'image prendra pour source l'url indiquée de chaque objet
    }
    createListItem.append(imageMovie); //Ajoute dans le li
    
    const divInListeItem = document.createElement('div');
    divInListeItem.classList.add('detail_movie');
    createListItem.append(divInListeItem);

    //Elements à mettre dans la div
    const titleMovie = document.createElement('h3'); //Créer dans le DOM un h3
    titleMovie.append(movieToShow.title); //Qui contiendra le titre du film
    divInListeItem.append(titleMovie);//Ajouter dans le li le nom du film
    
    const yearMovie = document.createElement('p');
    yearMovie.append(`🍿 Sorti en ${movieToShow.releaseYear}`);
    if(movieToShow.releaseYear != null || movieToShow.releaseYear != undefined){
        divInListeItem.append(yearMovie);
    }

    const durationMovie = document.createElement('p');
    durationMovie.append(`🎬 Durée du film : ${movieToShow.duration} min`)
    divInListeItem.append(durationMovie);

    const directorMovie = document.createElement('p');
    directorMovie.append(`📹 Réalisateur : ${movieToShow.director}`);
    divInListeItem.append(directorMovie);

    const actorsMovie = document.createElement('p');
    actorsMovie.append(`🤵‍♂️🤵‍♀️ Acteurs : ${movieToShow.actors}`);
    if(movieToShow.releaseYear != null || movieToShow.releaseYear != undefined){
        divInListeItem.append(actorsMovie);
    }

    const descriptionMovie = document.createElement('p');
    descriptionMovie.append(movieToShow.description);
    divInListeItem.append(descriptionMovie);

    const ratingOfMovie = document.createElement('p');
    ratingOfMovie.append(movieToShow.rating);
    ratingOfMovie.style.fontWeight = 'bold';
    divInListeItem.append(ratingOfMovie);
    //Fin des éléments à mettre dans la div

    //Ajout d'un bouton suppression du film
    const buttonDelete = document.createElement('button'); //On crée un bouton
    buttonDelete.setAttribute('id', 'delete_button'); // Ajouter un id à un élément
    buttonDelete.classList.add('btn', 'btn-dark'); //On ajoute les classes suivantes qui correspondent à Bootstrap
    buttonDelete.textContent = 'Supprimer'; //On ajoute du texte au bouton
    createListItem.append(buttonDelete); //On ajoute le bouton à chaque li

    //Action du bouton
    buttonDelete.addEventListener('click', () => {
        const itemToDelete = movieList.indexOf(movieToShow);
        movieList.splice(itemToDelete, 1);
        createList.removeChild(createListItem);

        console.log(movieList);
    });
}

//Ajouter dans cette liste chaque film
for (movie of movieList){ // Pour chaque film dans la liste
    movieShow(movie);
}

//Permettre au formulaire d'ajouter de nouveaux films
const form = document.getElementById('form_addMovie');
const button_submit = document.getElementById('button_submit');
let newMovie = [];

function addToList(event) {
    event.preventDefault();

    //Récupérer les valeurs du formulaire
    let movieNameValue = form.movieName.value;
    let movieDurationValue = parseFloat(form.movieDuration.value);
    let movieRealisatorValue = form.movieRealisator.value;
    let movieDescriptionValue = form.movieDescription.value;
    let movieRatingValue = parseFloat(form.movieRating.value);
    let movieLinkValue = form.movieLink.value;

    //Assigner en tant que nouvel object
    let newMovieItem = Object.assign(
        {
            title : movieNameValue,
            duration : movieDurationValue,
            director : movieRealisatorValue,
            description : movieDescriptionValue,
            rating : movieRatingValue,
            poster : movieLinkValue,
        }, newMovie
    );
    
    movieList.push(newMovieItem);
    movieShow(newMovieItem);
}
button_submit.addEventListener('click', addToList);
